package com.example.appproduct.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.example.appproduct.R;
import com.example.appproduct.database.DbHelper;

public class ListUserActivity extends AppCompatActivity {
    private DbHelper db;
    private Cursor cursor;
    private SimpleCursorAdapter adapter;

    public ListUserActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_product);

        db = new DbHelper(this);
        ListView lvProduct = (ListView) findViewById(R.id.lvProduct);

        cursor = db.getAllUsers();

        adapter = new SimpleCursorAdapter(this, R.layout.item_product, cursor, new String[]{
                DbHelper.ID, DbHelper.NAME, DbHelper.QUANTITY
        }, new int[]{
                R.id.tvID, R.id.tvName, R.id.tvQuantity
        }, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        lvProduct.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        cursor = db.getAllUsers();
        adapter.changeCursor(cursor);
        adapter.notifyDataSetChanged();
        db.close();
    }
}

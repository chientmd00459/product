package com.example.appproduct.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.appproduct.R;
import com.example.appproduct.database.DbHelper;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity  implements View.OnClickListener {

    private TextView btName;
    private TextView btQuantity;
    private Button btAdd;
    private Button btView;
    private DbHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

        db = new DbHelper(this);
        db.getReadableDatabase();

    }
    private void initView() {
        btName = (TextView) findViewById(R.id.tvName);
        btQuantity = (TextView) findViewById(R.id.tvQuantity);
        btAdd = (Button) findViewById(R.id.btAdd);
        btView = (Button) findViewById(R.id.btView);
        btAdd.setOnClickListener(this);
        btView = (Button) findViewById(R.id.btView);
        btView.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        if(view == btAdd) {
            onRegister();
        }
        if (view == btView){
            onGetAllProducts();
        }
    }

    private void onRegister() {
        if(btName.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please enter product name", Toast.LENGTH_LONG).show();
            return;
        }

        if(btQuantity.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please enter product quantity", Toast.LENGTH_LONG).show();
            return;
        }

        String isAdd = db.addUser(btName.getText().toString(), btQuantity.getText().toString());
        Toast.makeText(this, isAdd, Toast.LENGTH_LONG).show();
    }

    private void onGetAllProducts() {
        Intent intent = new Intent(MainActivity.this, ListUserActivity.class);
        startActivity(intent);
    }


}